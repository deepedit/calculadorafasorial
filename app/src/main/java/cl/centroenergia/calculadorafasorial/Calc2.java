package cl.centroenergia.calculadorafasorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Calc2 extends AppCompatActivity {

    double[] re;
    double[] im;
    boolean[] ModoCart;
    String ultlin;
    private static double precision = 0.0000001;

    TextView LReA;
    TextView LReB;
    TextView LReC;
    TextView LReD;
    TextView LReE;

    EditText TReA;
    EditText TReB;
    EditText TReC;
    EditText TReD;
    EditText TReE;

    TextView LImA;
    TextView LImB;
    TextView LImC;
    TextView LImD;
    TextView LImE;

    EditText TImA;
    EditText TImB;
    EditText TImC;
    EditText TImD;
    EditText TImE;

    TextView [] LRe;
    TextView [] LIm;
    EditText [] TRe;
    EditText [] TIm;

    EditText TOper;

    Button BConmA; //THis is the execution button


    String text[] = {
            "Calculadora de Fasores",	//0
            "Datos de los Fasores",		//1
            "Re",						//2
            "Im",						//3
            "|M|",						//4
            "<°",						//5
            "Graficar ", 				//6		//Para seleccionar si grafica o no
            "Sistema de Coordenadas",					//7
            "Expresión a calcular:",			//8
            "Acciones Generales",		//9
            "Graficar",					//10
            "Limpiar",					//11
            "Salir",					//12
            "Ayuda",					//13
            "index_esp.html",			//14
            "Representación Gráfica",	//15
            "Calcular",					//16
            "Ayuda en Línea",			//17
            "Calcular la operación escrita",		//18
            "Graficar los números seleccionados",	//19
            "Limpiar todos los números almacenados",		//20
            "Ayuda en línea (desde Internet)",		//21
            "Cambiar el Sistema de Coordenadas de la variable A: Cartesiano <-> Polar",	//22
            "Cambiar el Sistema de Coordenadas de la variable B: Cartesiano <-> Polar",	//23
            "Cambiar el Sistema de Coordenadas de la variable C: Cartesiano <-> Polar",	//24
            "Cambiar el Sistema de Coordenadas de la variable D: Cartesiano <-> Polar",	//25
            "Cambiar el Sistema de Coordenadas de la variable E: Cartesiano <-> Polar",	//26
            "Ingresar Ecuación"                     //27
    };

    //Button
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc2);

        LReA = (TextView) findViewById(R.id.LReA);
        LReB = (TextView) findViewById(R.id.LReB);
        LReC = (TextView) findViewById(R.id.LReC);
        LReD = (TextView) findViewById(R.id.LReD);
        LReE = (TextView) findViewById(R.id.LReE);

        TReA = (EditText) findViewById(R.id.TReA);
        TReB = (EditText) findViewById(R.id.TReB);
        TReC = (EditText) findViewById(R.id.TReC);
        TReD = (EditText) findViewById(R.id.TReD);
        TReE = (EditText) findViewById(R.id.TReE);

        LImA = (TextView) findViewById(R.id.LImA);
        LImB = (TextView) findViewById(R.id.LImB);
        LImC = (TextView) findViewById(R.id.LImC);
        LImD = (TextView) findViewById(R.id.LImD);
        LImE = (TextView) findViewById(R.id.LImE);

        TImA = (EditText) findViewById(R.id.TImA);
        TImB = (EditText) findViewById(R.id.TImB);
        TImC = (EditText) findViewById(R.id.TImC);
        TImD = (EditText) findViewById(R.id.TImD);
        TImE = (EditText) findViewById(R.id.TImE);

        TRe = new EditText[5];
        TIm = new EditText[5];
        LRe = new TextView[5];
        LIm = new TextView[5];

        TRe[0] = TReA;
        TIm[0] = TImA;
        LRe[0] = LReA;
        LIm[0] = LImA;

        TRe[1]=TReB;
        TIm[1]=TImB;
        LRe[1]=LReB;
        LIm[1]=LImB;

        TRe[2]=TReC;
        TIm[2]=TImC;
        LRe[2]=LReC;
        LIm[2]=LImC;

        TRe[3]=TReD;
        TIm[3]=TImD;
        LRe[3]=LReD;
        LIm[3]=LImD;

        TRe[4]=TReE;
        TIm[4]=TImE;
        LRe[4]=LReE;
        LIm[4]=LImE;

        re=new double[5];
        im=new double[5];
        ModoCart=new boolean[5];
        for(int i=0; i<5; i++){
            re[i]=0;
            im[i]=0;
            ModoCart[i]=true;
        }
        ultlin="";

        TOper = findViewById(R.id.TOper);
        TOper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = TOper.getText().toString();
                if (s!=null){
                    if (s.toLowerCase().contains(getString(R.string.ecuacion).toLowerCase())){
                        TOper.getText().clear();
                    }
                }
            }
        });

        //Boton de Ingresar Funcion:
        Button BFuncIngreso = (Button) findViewById(R.id.BFuncIngreso);
        BFuncIngreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BFuncIngresoActionPerformed();
            }
        });

        //Boton Calcular:
        Button BCalc = (Button) findViewById(R.id.BCalc);
        BCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BCalcActionPerformed();
            }
        });

        //Boton Sistema:
        Button BConmA = (Button) findViewById(R.id.BConmA);
        BConmA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i=0; i<5; i++){
                    BConmActionPerformed(i);
                }
            }
        });

        //Boton Limpiar:
        Button BReset = findViewById(R.id.BReset);
        BReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BResetActionPerformed();
            }
        });

        //Boton de Ayuda:
        Button BAyuda = findViewById(R.id.BAyuda);
        BAyuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        if (getIntent().hasExtra("ECUACION")) {
            String sEcuacion = getIntent().getStringExtra("ECUACION");
            TOper.setText(sEcuacion);
        }

    }

    //Ingreso de Ecuacion:
    private void BFuncIngresoActionPerformed(){
        Intent startCalc = new Intent(getApplicationContext(), Frame_calculadora_2.class);
        startActivity(startCalc);
    }

    //Calculo:
    private void TOperActionPerformed() {
        BCalcActionPerformed();
    }

    //Sistema de Coordinadas Cartesiano a Polar:
    private void BConmActionPerformed(int lt) {
        double retemp, imtemp, motemp, antemp;

        if (ModoCart[lt]){  //Cambiar de cartesiano a polar
            retemp=Double.parseDouble(TRe[lt].getText().toString());
            imtemp=Double.parseDouble(TIm[lt].getText().toString());

            motemp=java.lang.Math.sqrt(retemp*retemp+imtemp*imtemp);
            antemp=java.lang.Math.atan2(imtemp,retemp)/java.lang.Math.PI*180;

            if(motemp<precision && -precision<motemp) motemp=0;
            if(antemp<precision && -precision<antemp) antemp=0;

            LRe[lt].setText(text[4]);
            LIm[lt].setText(text[5]);

            TRe[lt].setText(cerca(motemp)+"");
            TIm[lt].setText(cerca(antemp)+"");

            ModoCart[lt]=false;
        } else{
            motemp=Double.parseDouble(TRe[lt].getText().toString());
            antemp=Double.parseDouble(TIm[lt].getText().toString());

            retemp=motemp*java.lang.Math.cos(antemp*java.lang.Math.PI/180);
            imtemp=motemp*java.lang.Math.sin(antemp*java.lang.Math.PI/180);

            if (retemp<precision && -precision<retemp) retemp=0;

            if(imtemp<precision && -precision<imtemp) imtemp=0;

            LRe[lt].setText("   "+text[2]);
            LIm[lt].setText("   "+text[3]);

            TRe[lt].setText(cerca(retemp)+"");
            TIm[lt].setText(cerca(imtemp)+"");

            ModoCart[lt]=true;
        }
    }

    //Resetea a los valores por defecto:
    private void BResetActionPerformed() {
        int i;
        for (i=0; i<5;i++ )
        {
            TRe[i].setText("0");
            TIm[i].setText("0");
            LRe[i].setText("  "+text[2]);
            LIm[i].setText("  "+text[3]);

            ModoCart[i]=true;
            re[i]=0;
            im[i]=0;
        }
        TOper.setText("");
    }

    //Muestra la ayuda
    private void BAyudaActionPerformed() {
        //Navegar a la web
    }

    public static double cerca(double in0)  // Transforma 5.999999999 en 6
    {
        String st;
        int signo;
        double in;
        double base;
        double dif;
        int i,l;

        signo=(in0>=0)?1:-1;
        in=in0*signo; //Aquí in0 es positivo

        //360 para transformar 1.25000000001 en 1.25

        base=(in*360000+0.4999999);
        dif=Math.abs( (in*360000)-Math.floor(base) );

        if (dif<precision && dif!=0)
        {
            in=Math.floor(in/precision*9+0.4999999)*precision/9;
        }

        in=in*signo;

        st=in+"";  //15
        l=st.length();

        if (l<15) return in;

        for (i=0; i<l; i++)
        {
            if (st.charAt(i)=='e'||st.charAt(i)=='E')
            {
                st=st.substring(0,15-(l-i))+st.substring(i,l);
                break;
            }
        }

        in=Double.parseDouble(st.substring(0,15));

        return in;
    }

    private void BCalcActionPerformed() {
        int lobj; //n° de la letra en la que se guarda el resultado
        String oper;  //igual a ingreso, pero ya sin letras
        String ingreso;  //Lo ingresado
        int i,l;
        char ch;
        double motemp, antemp;
        Complejo res;

        //Calcular todos los números en forma cartesiana
        for(i=0; i<5; i++){
            if(ModoCart[i]){
                re[i]=Double.parseDouble(TRe[i].getText().toString());
                im[i]=Double.parseDouble(TIm[i].getText().toString());

                if(re[i]<precision && -precision<re[i]) re[i]=0;
                if(im[i]<precision && -precision<im[i]) im[i]=0;
            }
            else{
                motemp=Double.parseDouble(TRe[i].getText().toString());
                antemp=Double.parseDouble(TIm[i].getText().toString());

                re[i]=motemp*java.lang.Math.cos(antemp*java.lang.Math.PI/180);
                im[i]=motemp*java.lang.Math.sin(antemp*java.lang.Math.PI/180);

                if(re[i]<precision && -precision<re[i]) re[i]=0;
                if(im[i]<precision && -precision<im[i]) im[i]=0;
            }
        }

        ingreso=TOper.getText().toString().toUpperCase();
        ultlin=ingreso;
        l=ingreso.length();

        switch (ingreso.charAt(0))
        {
            case 'a':
            case 'A':
                lobj=0;
                break;
            case 'b':
            case 'B':
                lobj=1;
                break;
            case 'c':
            case 'C':
                lobj=2;
                break;
            case 'd':
            case 'D':
                lobj=3;
                break;
            case 'e':
            case 'E':
                lobj=4;
                break;
            default:
                lobj=0;
        }

        oper="";

        for(i=2; i<l; i++)  //Aqui se reemplazan las variables por sus valores
        {
            ch=ingreso.charAt(i);

            switch(ch){
                case 'a':
                case 'A':
                    if(i<l-1)  //detecta "arccos", "arc..."
                    {
                        if (ingreso.charAt(i+1)=='r' || ingreso.charAt(i+1)=='R')
                        {
                            oper=oper+(ch+"");
                            continue;
                        }
                    }
                    oper=oper+"(";
                    oper=oper+re[0];
                    oper=oper+"+";
                    oper=oper+im[0];
                    oper=oper+"*i)";
                    break;

                case 'b':
                case 'B':
                    oper=oper+"(";
                    oper=oper+re[1];
                    oper=oper+"+";
                    oper=oper+im[1];
                    oper=oper+"*i)";
                    break;

                case 'c':
                case 'C':
                    if(i>2)  //detecta "arccos", "arc..."
                    {
                        if (ingreso.charAt(i-1)=='r' || ingreso.charAt(i-1)=='R')
                        {
                            oper=oper+(ch+"");
                            continue;
                        }
                    }
                    if(i<l-1)  //detecta "cos", "cosh"
                    {
                        if (ingreso.charAt(i+1)=='o' || ingreso.charAt(i+1)=='O')
                        {
                            oper=oper+(ch+"");
                            continue;
                        }
                    }
                    oper=oper+"(";
                    oper=oper+re[2];
                    oper=oper+"+";
                    oper=oper+im[2];
                    oper=oper+"*i)";
                    break;

                case 'd':
                case 'D':
                    oper=oper+"(";
                    oper=oper+re[3];
                    oper=oper+"+";
                    oper=oper+im[3];
                    oper=oper+"*i)";
                    break;

                case 'e':
                case 'E':
                    if(i>2)  //detecta forma tipo 2.36E+4 (notación científica)
                    {
                        if (ingreso.charAt(i-1)>='0' && ingreso.charAt(i-1)<='9')
                        {
                            oper=oper+(ch+"");
                            continue;
                        }
                    }

                    if(i>2)  //detecta "sen", "senh"
                    {
                        if (ingreso.charAt(i-1)=='s' || ingreso.charAt(i-1)=='S')
                        {
                            oper=oper+(ch+"");
                            continue;
                        }
                    }
                    if(i<l-1)  //detecta "exp"
                    {
                        if (ingreso.charAt(i+1)=='x' || ingreso.charAt(i+1)=='X')
                        {
                            oper=oper+(ch+"");
                            continue;
                        }
                    }
                    oper=oper+"(";
                    oper=oper+re[4];
                    oper=oper+"+";
                    oper=oper+im[4];
                    oper=oper+"*i)";
                    break;

                default:
                    oper=oper+(ch+"");
            }
        }

        //arbol=new Nodo(oper);
        //res=arbol.evalua();

        res=new Complejo(oper);

        if (!ModoCart[lobj]){
            BConmActionPerformed(lobj);
        }

        TRe[lobj].setText(cerca(res.re)+"");
        TIm[lobj].setText(cerca(res.im)+"");

    }

}
