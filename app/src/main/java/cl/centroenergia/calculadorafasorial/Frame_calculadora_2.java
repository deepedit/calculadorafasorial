package cl.centroenergia.calculadorafasorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Frame_calculadora_2 extends AppCompatActivity {


    private int posCursor=0;
    String[] idiom = {"Borrar",         //0
            "Cursor a la Izquierda",                        //1
            "Cursor a la Derecha",                          //2
            "Aceptar",                          //3
            "Cancelar",                         //4
            "seno",                             //5
            "coseno",                           //6
            "seno hiperbólico",                 //7
            "coseno hiperbólico",               //8
            "potencia",                         //9
            "exponencial",                      //10
            "logaritmo natural",                //11
            "conjugado",                        //12
            "Acepta la ecuación",//13
            "Editor de Ecuaciones",             //14
            "Unidad imaginaria",                  //15
            "Ángulo [grados]",                  //16
            "Notación Científica (2E3 = 2x10^3)",//17
    };

    private Button Button_0;
    private Button Button_1;
    private Button Button_2;
    private Button Button_3;
    private Button Button_4;
    private Button Button_5;
    private Button Button_6;
    private Button Button_8;
    private Button Button_9;
    private Button Button_A;
    private Button Button_Aceptar;
    private Button Button_B;
    private Button Button_C;
    private Button Button_Cient;
    private Button Button_D;
    private Button Button_E;
    private Button Button_Im;
    private Button Button_Re;
    private Button Button_adic;
//    private Button Button_cancelar;
    private Button Button_conj;
    private Button Button_cos;
    private Button Button_cosh;
    private Button Button_decimal;
    private Button Button_delete;
    private Button Button_der;
    private Button Button_div;
    private Button Button_equal;
    private Button Button_exp;
    private Button Button_izq;
    private Button Button_ln;
    private Button Button_mult;
    private Button Button_pow;
    private Button Button_sin;
    private Button Button_sinh;
    private Button Button_sustr;
    private EditText TextField_formula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_calculadora_2);
        initComponents();
    }

    private void initComponents() {

        TextField_formula = (EditText) findViewById(R.id.TextField_formula);
        TextField_formula.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                TextField_formula_focusGained();
            }
        });
        TextField_formula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextField_formula_mouseClicked();
            }
        });

        Button_izq = (Button) findViewById(R.id.Button_izq);

        Button_izq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonMov_mouseClicked("izq");
            }
        });

        Button_delete = (Button) findViewById(R.id.Button_delete);
        Button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonMov_mouseClicked("del");
            }
        });

        Button_der = findViewById(R.id.Button_der);
        Button_der.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonMov_mouseClicked("der");
            }
        });

        Button_Re = (Button) findViewById(R.id.Button_Re);
        Button_Re.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("i");
            }
        });
        Button_Im = (Button) findViewById(R.id.Button_Im);
        Button_Im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("<");
            }
        });
        Button_sin = (Button) findViewById(R.id.Button_sin);
        Button_sin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonFunctions_mouseClicked("sin()");
            }
        });
        Button_cos = (Button) findViewById(R.id.Button_cos);
        Button_cos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonFunctions_mouseClicked("cos()");
            }
        });
        Button_sinh = (Button) findViewById(R.id.Button_sinh);
        Button_sinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonFunctions_mouseClicked("sinh()");
            }
        });
        Button_cosh = (Button) findViewById(R.id.Button_cosh);
        Button_cosh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonFunctions_mouseClicked("cosh()");
            }
        });
        Button_pow = (Button) findViewById(R.id.Button_pow);
        Button_pow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("^");
            }
        });
        Button_exp = (Button) findViewById(R.id.Button_exp);
        Button_exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonFunctions_mouseClicked("exp()");
            }
        });
        Button_ln = (Button) findViewById(R.id.Button_ln);
        Button_ln.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonFunctions_mouseClicked("ln()");
            }
        });
        Button_conj = (Button) findViewById(R.id.Button_conj);
        Button_conj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("'");
            }
        });

        Button_1 = (Button) findViewById(R.id.Button_1);
        Button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("1");
            }
        });
        Button_2 = (Button) findViewById(R.id.Button_2);
        Button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("2");
            }
        });
        Button_3 = (Button) findViewById(R.id.Button_3);
        Button_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("3");
            }
        });
        Button_4 = (Button) findViewById(R.id.Button_4);
        Button_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("4");
            }
        });
        Button_5 = (Button) findViewById(R.id.Button_5);
        Button_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("5");
            }
        });
        Button_6 = (Button) findViewById(R.id.Button_6);
        Button_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("6");
            }
        });
        Button button_7 = (Button) findViewById(R.id.Button_7);
        button_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("7");
            }
        });
        Button_8 = (Button) findViewById(R.id.Button_8);
        Button_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("8");
            }
        });
        Button_9 = (Button) findViewById(R.id.Button_9);
        Button_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("9");
            }
        });
        Button_Cient = (Button) findViewById(R.id.Button_Cient);
        Button_Cient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("E");
            }
        });
        Button_0 = (Button) findViewById(R.id.Button_0);
        Button_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("0");
            }
        });
        Button_decimal = (Button) findViewById(R.id.Button_decimal);
        Button_decimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked(".");
            }
        });

        Button_div = (Button) findViewById(R.id.Button_div);
        Button_div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("/");
            }
        });
        Button_mult = (Button) findViewById(R.id.Button_mult);
        Button_mult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("*");
            }
        });
        Button_sustr = (Button) findViewById(R.id.Button_sustr);
        Button_sustr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("-");
            }
        });
        Button_adic = (Button) findViewById(R.id.Button_adic);
        Button_adic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("+");
            }
        });
        Button_equal = (Button) findViewById(R.id.Button_equal);
        Button_equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("=");
            }
        });
        Button_A = (Button) findViewById(R.id.Button_A);
        Button_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("A");
            }
        });
        Button_B = (Button) findViewById(R.id.Button_B);
        Button_B.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("B");
            }
        });
        Button_C = (Button) findViewById(R.id.Button_C);
        Button_C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("C");
            }
        });
        Button_D = (Button) findViewById(R.id.Button_D);
        Button_D.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("D");
            }
        });
        Button_E = (Button) findViewById(R.id.Button_E);
        Button_E.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonCentral_mouseClicked("E");
            }
        });
        Button_Aceptar = (Button) findViewById(R.id.Button_Aceptar);
        Button_Aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button_Aceptar_mouseClicked();
            }
        });
        //Button_cancelar = (Button) findViewById(R.id.Button_delete);

    }

//    private void caretUpdate(javax.swing.event.CaretEvent evt) {
//        //TextField_formula.setCaretPosition(TextField_formula.getCaretPosition());
//    }

    private void Button_Aceptar_mouseClicked() {
        //this.p.setText(TextField_formula.getText());
        //this.setVisible(false);
        Intent startCalc = new Intent(getApplicationContext(), Calc2.class);
        startCalc.putExtra("ECUACION", TextField_formula.getText().toString());
        startActivity(startCalc);
    }

    private void TextField_formula_focusGained() {
        TextField_formula_seleccionada();
    }

    private void TextField_formula_mouseClicked() {
        TextField_formula_seleccionada();
    }

    private void TextField_formula_seleccionada(){
        //posCursor=TextField_formula.getCaretPosition();
        posCursor=TextField_formula.getSelectionStart();
    }

    private void ButtonFunctions_mouseClicked(String evt) {
        String texto=TextField_formula.getText().toString();
        String texto_antes, texto_desp;
        texto_antes=texto.substring(0, posCursor);
        texto_desp=texto.substring(posCursor);
        TextField_formula.setText(texto_antes + evt + texto_desp);
        posCursor=posCursor+evt.length()-1;
        //TextField_formula.setCaretPosition(posCursor);
        TextField_formula.setSelection(posCursor);
    }

    private void ButtonMov_mouseClicked(String evt) {
        if(evt == "der"){
            if(posCursor<TextField_formula.getText().length()) posCursor+=1;
        }
        else if(evt == "izq"){
            if(posCursor>0) posCursor-=1;
        }
        else if(posCursor>0){
            String texto=TextField_formula.getText().toString();
            String texto_antes, texto_desp;
            texto_antes=texto.substring(0, posCursor-1);
            texto_desp=texto.substring(posCursor);
            TextField_formula.setText(texto_antes + texto_desp);
            posCursor-=1;
        }
        //TextField_formula.setCaretPosition(posCursor);
        TextField_formula.setSelection(posCursor);
    }

    private void ButtonCentral_mouseClicked(String evt) {
        String texto=TextField_formula.getText().toString();
        String texto_antes, texto_desp;
        texto_antes=texto.substring(0, posCursor);
        texto_desp=texto.substring(posCursor);
        TextField_formula.setText(texto_antes + evt + texto_desp);
        posCursor=posCursor+evt.length();
        TextField_formula.setSelection(posCursor);
        //TextField_formula.setCaretPosition(posCursor);
    }

//    private void Button_cancelar_mouseClicked(java.awt.event.MouseEvent evt) {
//        this.setVisible(false);
//    }


}
