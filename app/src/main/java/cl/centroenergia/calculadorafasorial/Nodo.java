package cl.centroenergia.calculadorafasorial;

public class Nodo{
	public String texto;
	public Nodo der;
	public Nodo izq;
	char op;

	public Nodo(String xx){
		String x=xx;
		String y="";
		int i;
		int l=xx.length();
		int npar=0;
		boolean error;
                
		///////Detector de -- , +- , ...
		do {
                    error=false;
                    y=x;
                    x="";
                    l=y.length();
                    for(i=0; i<l-1; i++){
                        error=true;
                        if (y.charAt(i)=='-' && y.charAt(i+1)=='-'){
                            x=x+"+";
                            i++;
                            continue;
                        }
                        else if (y.charAt(i)=='-' && y.charAt(i+1)=='+'){
                            x=x+"-";
                            i++;
                            continue;
                        }
                        else if (y.charAt(i)=='+' && y.charAt(i+1)=='+'){
                            x=x+"+";
                            i++;
                            continue;
                        }
                        else if (y.charAt(i)=='+' && y.charAt(i+1)=='-'){
                            x=x+"-";
                            i++;
                            continue;
                        }
                        else if (y.charAt(i)=='*' && y.charAt(i+1)=='+'){
                            x=x+"*";
                            i++;
                            continue;
                        }
                        else if (y.charAt(i)=='/' && y.charAt(i+1)=='+'){
                            x=x+"/";
                            i++;
                            continue;
                        }
                        else {
                            x=x+y.charAt(i);
                            error=false;
                        }
                    }
                    x=x+y.charAt(l-1);
		} while (error);

		y="";
		l=x.length();
		der=null;
		izq=null;
		op=' ';
		texto="";

		// Creacion de nodos para para suma/resta
		for (i=l-1;i>=0;i--){
                    if (x.charAt(i)=='('){
                        npar++;
                    }
                    if (x.charAt(i)==')'){
                        npar--;
                    }
                    if (x.charAt(i)=='+' && npar==0){
                        if (i>1){
                            if (x.charAt(i-2)>='0' && x.charAt(i-2)<='9' && x.charAt(i-1)=='E'){
                                continue;
                            }
                        }
                        if (i>0){
                            der=new Nodo(x.substring(0,i));
                        }
                        else{
                            der=new Nodo("0");
                        }
                        izq=new Nodo(x.substring(i+1,l));
                        op='+';
                        return;
                    }
                    if (x.charAt(i)=='-' && npar==0){
                        if (i>1){
                            if (x.charAt(i-2)>='0' && x.charAt(i-2)<='9' && x.charAt(i-1)=='E'){
                                continue;
                            }
                        }
                        if (i>0){
                            if (x.charAt(i-1)=='*' || x.charAt(i-1)=='/' || x.charAt(i-1)=='<'){
                                continue;
                            }
                            der=new Nodo(x.substring(0,i));
                        }
                        else{
                            der=new Nodo("0");
                        }
                        izq=new Nodo(x.substring(i+1,l));
                        op='-';
                        return;
                    }
		}
		npar=0;

		// Creacion de nodos para multiplicion/division
		for (i=l-1;i>=0;i--){
                    if (x.charAt(i)=='('){
                        npar++;
                    }

                    if (x.charAt(i)==')'){
                        npar--;
                    }

                    if (x.charAt(i)=='*' && npar==0){
                        der=new Nodo(x.substring(0,i));
                        izq=new Nodo(x.substring(i+1,l));
                        op='*';
                        return;
                    }
                    
                    if (x.charAt(i)=='/' && npar==0){
                            der=new Nodo(x.substring(0,i));
                            izq=new Nodo(x.substring(i+1,l));
                            op='/';
                            return;
                    }
		}

		// Creacion de nodos para forma polar mod<ang
		for (i=l-1;i>=0;i--){
                    if (x.charAt(i)=='('){
                        npar++;
                    }

                    if (x.charAt(i)==')'){
                        npar--;
                    }

                    if (x.charAt(i)=='<' && npar==0){
                        izq=new Nodo(x.substring(0,i));
                        der=new Nodo(x.substring(i+1,l));
                        op='<';
                        return;
                    }
		}

		// Creacion de nodos para potencia
		for (i=l-1;i>=0;i--){
                    if (x.charAt(i)=='('){
                        npar++;
                    }
                    if (x.charAt(i)==')'){
                        npar--;
                    }
                    if (x.charAt(i)=='^' && npar==0){
                        izq=new Nodo(x.substring(0,i));
                        der=new Nodo(x.substring(i+1,l));
                        op='^';
                        return;
                    }
		}

		// Creacion de nodos para conjugado
		if (x.charAt(l-1)=='\"' | x.charAt(l-1)=='\'' | x.charAt(l-1)=='_'){
                    op='_';
                    izq=new Nodo("0");
                    der=new Nodo(x.substring(0,l-1));
                    texto="";
                    return;
		}
	
		// Creacion de nodos para grados
		if (x.charAt(l-1)=='�' | x.charAt(l-1)=='�'){
                    op='�';
                    izq=new Nodo("0");
                    der=new Nodo(x.substring(0,l-1));
                    texto="";
                    return;
		}

		if (l>=2)
		if (x.charAt(0)=='(' && x.charAt(l-1)==')'){
                    op='+';
                    izq=new Nodo("0");
                    der=new Nodo(x.substring(1,l-1));
                    texto="";
                    return;
		}

		/////// Trigonometria y exponenciales
		// El texto se transformo a mayuscula antes de entrar aqui

		//pi
		if (l==2)
		if (x.substring(0,2).compareTo("PI")==0){
			op='p';
			izq=new Nodo("0");
			der=new Nodo("0");
			texto="";
			return;
		}
		
		//funciones de 2 letras: ln
		if (l>3)
                    if (x.substring(0,3).compareTo("LN(")==0 && x.charAt(l-1)==')'){
                        op='l';
                        izq=new Nodo("0");
                        der=new Nodo(x.substring(3,l-1));
                        texto="";
                        return;
                    }
		
		//funciones de 3 letras: cos, sin, exp
		if (l>4){
                    if (x.substring(0,4).compareTo("COS(")==0 && x.charAt(l-1)==')'){
			op='c';
			izq=new Nodo("0");
			der=new Nodo(x.substring(4,l-1));
			texto="";
			return;
                    }
                    if (x.substring(0,4).compareTo("SIN(")==0 && x.charAt(l-1)==')'){
			op='s';
			izq=new Nodo("0");
			der=new Nodo(x.substring(4,l-1));
			texto="";
			return;
                    }
                    if (x.substring(0,4).compareTo("SEN(")==0 && x.charAt(l-1)==')'){
			op='s';
			izq=new Nodo("0");
			der=new Nodo(x.substring(4,l-1));
			texto="";
			return;
                    }
                    if (x.substring(0,4).compareTo("EXP(")==0 && x.charAt(l-1)==')'){
			op='e';
			izq=new Nodo("0");
			der=new Nodo(x.substring(4,l-1));
			texto="";
			return;
                    }
                }

		//Funciones con 4 letras: cosh, sinh
		if (l>5){
                    if (x.substring(0,5).compareTo("SINH(")==0 && x.charAt(l-1)==')'){
                        op='S';
                        izq=new Nodo("0");
                        der=new Nodo(x.substring(5,l-1));
                        texto="";
                        return;
                    }
                    if (x.substring(0,5).compareTo("SENH(")==0 && x.charAt(l-1)==')'){
                        op='S';
                        izq=new Nodo("0");
                        der=new Nodo(x.substring(5,l-1));
                        texto="";
                        return;
                    }
                    if (x.substring(0,5).compareTo("COSH(")==0 && x.charAt(l-1)==')'){
                        op='C';
                        izq=new Nodo("0");
                        der=new Nodo(x.substring(5,l-1));
                        texto="";
                        return;
                    }
                }

		//funciones de 6 letras: arccos, arcsin, SOLO CONSIDERA PARTE REAL
		if (l>7){
		if (x.substring(0,7).compareTo("ARCCOS(")==0 && x.charAt(l-1)==')'){
                    op='1';
                    izq=new Nodo("0");
                    der=new Nodo(x.substring(7,l-1));
                    texto="";
                    return;
		}
		if (x.substring(0,7).compareTo("ARCSIN(")==0 && x.charAt(l-1)==')'){
                    op='2';
                    izq=new Nodo("0");
                    der=new Nodo(x.substring(7,l-1));
                    texto="";
                    return;
		}
                if (x.substring(0,7).compareTo("ARCSEN(")==0 && x.charAt(l-1)==')'){
                    op='2';
                    izq=new Nodo("0");
                    der=new Nodo(x.substring(7,l-1));
                    texto="";
                    return;
                }
            }
            texto=x;
	}
	
	
	public Complejo evalua(){
		Complejo n1,n2;
		n1=new Complejo(0,0);
		n2=new Complejo(0,0);
		if (this.der==null && this.izq==null)
		{
		}
		else if (this.der==null){
                    n1=izq.evalua();
		}
		else if (izq==null){
                    n1=der.evalua();
		}
		else{
                    n1=der.evalua();
                    n2=izq.evalua();
		}
			
		if (op=='+'){
                    return n1.mas(n2);
		}	
			
		else if (op=='-'){
                    return n1.menos(n2);
		}

		else if (op=='*'){
                    return n1.por(n2);
		}

		else if (op=='/'){
                    return n1.div(n2);
		}

		else if (op=='<'){
                    //n2 contiene un complejo, n1.re contiene el angulo de giro a sumarle
                    double x,y;
                    x=n2.re*Math.cos(n1.re*Math.PI/180)-n2.im*Math.sin(n1.re*Math.PI/180);
                    y=n2.re*Math.sin(n1.re*Math.PI/180)+n2.im*Math.cos(n1.re*Math.PI/180);
                    return new Complejo(x,y);
                    // n1.div(n2);
		}

		else if(op=='�'){
                    return new Complejo(n1.re*Math.PI/180,n1.im*Math.PI/180);
		}

		else if(op=='p'){ //pi
                    return new Complejo(Math.PI,0);
		}

		else if (op=='^'){  //potencia
                    //n2: base compleja,  n1: exponente complejo
                    return Complejo.pow(n2,n1);
		}
		
		else if (op=='e'){ //exponencial
                    return Complejo.exp(n1);
		}
		
		else if (op=='c'){ //coseno
                    return Complejo.cos(n1);
		}
		
		else if (op=='s'){ //seno
                    return Complejo.sin(n1);
		}
		
		else if (op=='C'){ //coseno hiperbolico
                    return Complejo.cosh(n1);
		}
		
		else if (op=='S'){ //seno hiperbolico
                    return Complejo.sinh(n1);
		}

		else if (op=='l'){ //logaritmo natural
                    return Complejo.ln(n1);
		}
		
		else if (op=='1'){ //arcocoseno, SOLO PARTE REAL
                    return new Complejo(Math.acos(n1.re),0);
		}

		else if (op=='2'){ //arcoseno, SOLO PARTE REAL
                    return new Complejo(Math.asin(n1.re),0);
		}

		else if (op=='_'){
                    n1.conjuga();
                    return n1;
		}

		else return new Complejo(texto,0);
	}
	
	
	
}